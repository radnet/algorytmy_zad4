﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zad4
{
    class Program
    {
        int infinite = int.MaxValue;

        static void Main(string[] args)
        {

            Program pro = new Program();
            pro.readData();

        }

        private void readData()
        {
            int testCount = Convert.ToInt32(Console.ReadLine());

            for (int t = 0; t < testCount; t++)
            {
                string[] nodeEdgeCount = Console.ReadLine().Split(' ');

                int nodeCount = Convert.ToInt32(nodeEdgeCount[0]);
                int edgeCount = Convert.ToInt32(nodeEdgeCount[1]);

                int[,] w = new int[nodeCount, nodeCount];

                for (int i = 0; i < nodeCount; i++)
                {
                    for (int j = 0; j < nodeCount; j++)
                    {
                        w[i, j] = infinite;
                    }
                }

                for (int i = 0; i < edgeCount; i++)
                {
                    string[] nodeWeight = Console.ReadLine().Split(' ');

                    int nodeX = Convert.ToInt32(nodeWeight[0]);
                    int nodeY = Convert.ToInt32(nodeWeight[1]);
                    int weight = Convert.ToInt32(nodeWeight[2]);

                    w[nodeX - 1, nodeY - 1] = weight;
                }

                FordBellman fb = new FordBellman(t, w, nodeCount);
                fb.start();

            }

        }
    }

    class FordBellman
    {
        int ID;
        int[,] A;
        int N;
        int infinite = int.MaxValue;
        int[] D;
        int[] PI;

        public FordBellman(int ID, int[,] A, int nodeCount)
        {
            this.ID = ID + 1;
            this.A = A;
            this.N = nodeCount;
        }

        public void init()
        {
            D = new int[N];
            PI = new int[N];
            for (int v = 0; v < N; v++)
            {
                D[v] = A[0,v];
                PI[v] = -1;
            }
        }

        public void start()
        {
            init();
            for (int k = 0; k < N - 2; k++)
            {
                for (int v = 1; v < N; v++)
                {
                    for (int u = 0; u < N; u++)
                    {
                        if (D[u] == infinite || A[u, v] == infinite)
                        {
                            continue;
                        }

                        if (D[v] > D[u] + A[u, v])
                        {
                            D[v] = D[u] + A[u, v];
                            PI[v] = u;
                        }
                    }
                }
            }

            output();
        }

        void output()
        {
            Console.WriteLine("Graf nr " + ID);
            for (int i = 1; i < N; i++)
            {
                if (D[i] == infinite)
                {
                    Console.WriteLine("NIE ISTNIEJE DROGA Z 1 DO " + (i + 1) );
                    continue;
                }

                string output = "1";

                output += getPath(i);
                output += " " + D[i];

                Console.WriteLine(output);
            }
        }

        private string getPath(int v)
        {
            if (PI[v] == -1)
                return "-" + (v + 1);
            else
                return getPath(PI[v]) + "-" + (v + 1);
        }
    }
}
